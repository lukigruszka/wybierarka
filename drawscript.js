$(document).ready(function() {
	$.ajax({
        type: "GET",
        url: "maps/mod20.svg",
        dataType: "xml",
        success: function(svgData) {
			$.getJSON("regions/identyfikatory.json", function(jsonData){processXML(svgData, jsonData);})
		}
    });
	
	var allEmployees = [];
	var chosenEmployees = [];
	var peopleFromChosenDep = [];
	var visibleEmployees = [];

	var regionsIdentifiers = {};
	var regions = {};
	var markedRegions = [];
	var cities = {};
	var numOfRegionsInWoj = {};
	
	var displayMode =  true;
	
	/* ------ Funkcja główna ------ */
	function processXML(xml, json){
		regionsIdentifiers = json;
		$.when(getEmployeesFromDatabase()).then(function(result){
			for(employee in result){
				result[employee]["rID"] = result[employee]["rID"].split(';');
				result[employee]["tmp_rID"] = result[employee]["rID"];
			}
			allEmployees = result;
			if(!displayMode){
				$('#displayWholeToolBox').css('display', 'none');
				$('#editionWholeToolBox').css('display', 'block');
			}
			else {
				$('#displayWholeToolBox').css('display', 'block');
				$('#editionWholeToolBox').css('display', 'none');
			}
			visibleEmployees = allEmployees.slice();
			$('#searchBox input').val('');
			$('#chooseDep select').trigger("change");
		}, function(e){
			alert("Pobieranie użytkowników nie pykło");
			console.log(e);
		});
		
		createMap(xml);
		createMapWithCities();
		handleMapEvents();
		fillDepSelect();
		
		layoutFix();
	}
	
	/*----- Tworzenie mapy -----*/
	function createMap(xml){
		var container = document.getElementById("center");
		var map = new Raphael(container, mapWidth, mapHeight);
		map.setViewBox(150, -800, mapWidth, mapHeight);
		$(xml).find('path').each(function(){
			curID = ($(this).attr('id'));
			regions[curID] = map.path($(this).attr('d')).attr(style).transform("m1,0,0,-1,0,0").hide();
			if(curID.split('.')[0] == '2')
				regions[curID].attr(woj);
			else
				regions[curID].attr(pow);
			regions[curID].id = curID;
			
			if(curID.split('.')[0] != 2){
				var curWojID = curID.split('.')[2];
				if(numOfRegionsInWoj[curWojID] == undefined)
					numOfRegionsInWoj[curWojID] = 1;
				else
					numOfRegionsInWoj[curWojID]++;
			}
		});
	}
	function createMapWithCities(){
		$.each(regionsIdentifiers, function(regID, regProperties){
			if(regID[0] == '4' && regProperties['name'][0] == regProperties['name'][0].toUpperCase()){
				cities[regID] = regions[regID];
			}
		});
	}
	
	/*----- Reakcje regionów na gesty myszy -----*/	
	function handleMapEvents(){
		for(regionID in regions){
			(function(curID){
				if(curID.split('.')[0] == '2')
					regions[curID].show();
				
				regions[curID].mousemove(function(mouse){
					if(displayMode)
						return;
					if(regions[curID].attr("fill-opacity") != '0')
						$(".tooltip").text(regionsIdentifiers[curID]["name"]).css({
							"display": "block",
							"left": mouse.pageX-$(".tooltip").outerWidth()-1,
							"top": mouse.pageY-$(".tooltip").outerHeight()-1
						});
				});
				
				regions[curID].mouseover(function(){
					if(displayMode)
						return;
					if(regions[curID].attr("fill-opacity") != '0')
						regions[curID].attr(hover);
				});
				
				regions[curID].mouseout(function(){
					if(displayMode)
						return;
					if(regions[curID].attr("fill-opacity") != '0'){
						$(".tooltip").css("display", "none");
						if(markedRegions.includes(curID))
							regions[curID].attr(chosen);
						else
							regions[curID].attr(unchosen);
					}
				});
				
				regions[curID].click(function(){
					if(displayMode)
						return;
					if(curID.split('.')[0] == '4'){
						if(markedRegions.includes(curID)){
							markedRegions.splice(markedRegions.indexOf(curID), 1);
							var markedAnother = false;
							for(regionIndex in markedRegions)
								if(markedRegions[regionIndex].split('.')[2] == curID.split('.')[2]){
									markedAnother = true;
									break;
								}
							if(!markedAnother){
								for(subregionID in regions)
									if(subregionID.split('.')[2] == curID.split('.')[2]){
										regions[subregionID].hide();
										if(subregionID.split('.')[0] == '2'){
											regions[subregionID].show();
											regions[subregionID].attr(fillVisible);
										}
									}
							}
						}
						else{
							markedRegions.push(curID);
						}
					}
					else if(curID.split('.')[0] == '2'){
						for(subregionID in regions)
							if(subregionID.split('.')[2] == curID.split('.')[2])
								regions[subregionID].show();
						regions[curID].attr(fillInvisible);
					}
					if(chosenEmployees[0] === undefined)
						alert("Zmień tryb lub wybierz użytkownika")
					else{
						chosenEmployees[0]['tmp_rID'] = markedRegions.slice();
					}
					updateLeftColumn(chosenEmployees);
				});
				
				regions[curID].dblclick(function(){
					if(displayMode)
						return;
					if(curID.split('.')[0] == '4'){
						var markedAll = true;
						for(subregionID in regions){
							if(subregionID.split('.')[0] == '2')
								continue;
							if(subregionID.split('.')[2] == curID.split('.')[2] && !markedRegions.includes(subregionID)){
								markedAll = false;
								break;
							}
						}
						if(markedAll){
							for(subregionID in regions)
								if(subregionID.split('.')[2] == curID.split('.')[2]){
									if(markedRegions.includes(subregionID))
										markedRegions.splice(markedRegions.indexOf(subregionID), 1);
									regions[subregionID].attr(unchosen).hide();
									if(subregionID.split('.')[0] == '2'){
										regions[subregionID].show();
										regions[subregionID].attr(fillVisible);
									}
								}
						}
						else{
							for(subregionID in regions)
								if(subregionID.split('.')[2] == curID.split('.')[2]){
									if(!(markedRegions.includes(subregionID)) && subregionID.split('.')[0] != '2')
										markedRegions.push(subregionID);
									regions[subregionID].attr(chosen).show();
									if(subregionID.split('.')[0] == '2')
										regions[subregionID].attr(fillInvisible);
								}
						}
					}
					if(chosenEmployees[0] === undefined)
						alert("Zmień tryb lub wybierz użytkownika")
					else
						chosenEmployees[0]['tmp_rID'] = markedRegions.slice();
					updateLeftColumn(chosenEmployees);
				});
			})(regionID);
		}
	}
	
	/* ------ Ajax ------ */
	function getEmployeesFromDatabase(){
		return $.ajax({
			type: "GET",
			url: "get-users.php",
			contentType: "application/json; charset=utf-8",
			dataType: 'json'
		});
	}
	function sendEmployeeToDatabase(employee, deleteEmployee = false){
	var xhttp = new XMLHttpRequest();
	var file;
	if(deleteEmployee)
		file = "del-user.php";
	else
		file = "send-user.php";
	employee['rID'] = employee['rID'].join(';');
	
	xhttp.open("POST", file, true);
	xhttp.setRequestHeader("Content-type", "application/json");
	xhttp.send(JSON.stringify(employee));
	
	employee['rID'] = employee['rID'].split(';');
	}
	function getPostalCodes(){
		return {
			type: "GET",
			url: "./regions/kody.json",
			contentType: "application/json; charset=utf-8",
			dataType: 'json'
		};
	}
	
	/* ------ Lewa kolumna ------ */
	function updateLeftColumn(employees){
		$('#chosenEmployees').empty();
		if(employees == undefined)
			$('#chosen').css('display', 'hidden'); 
		else
			$('#chosen').css('display', 'block');
		for(employee in employees){
			var cur_rID = employees[employee]['tmp_rID'];
			if($('#e' + employees[employee]['eID']).length == 0)
				$('#chosenEmployees').append('<li class="employee" id="e' + employees[employee]['eID'] + '">' + employees[employee]['name'] + ' ' + employees[employee]['surname'] + '</li>');
			createListWithSupRegions(cur_rID, employees[employee]['eID']);
		}
	}
	function createListWithSupRegions(rID, eID){
		emp = $("#e"+eID);
		if(!emp.length)
			alert("Błąd stworzonego użytkownika w bocznym panelu. Szukane eID: " + eID);
		emp.find('ul').empty()
		emp.append('<ul class="supRegion">');
		var empSup = emp.find('ul');
		
		var chosenWoj = {};
		for(regionID in rID){
			if(regionsIdentifiers[rID[regionID]] == null)
				break;
			var wojName, wojID;
			for(seekWoj in regionsIdentifiers){
				var curRegIDSplitted = seekWoj.split('.');
				if(curRegIDSplitted[0] == 2 && curRegIDSplitted[2] == rID[regionID].split('.')[2]){
					wojName = regionsIdentifiers[seekWoj]["name"];
					wojID = seekWoj;
					break;
				} 
			}
			if(emp.find($("li:contains('"+wojName+"')")).length == 0){
				empSup.append('<li>'+wojName+'<ul class="subRegion" /></li>');
				chosenWoj[wojID] = regionsIdentifiers[wojID];
			}
			empSub = empSup.find($("li:contains('"+wojName+"')")).find('ul');
			empSub.append('<li>'+regionsIdentifiers[rID[regionID]]["name"]+'</li>');
		}
		emp.append('</ul>');
		
		checkForFullness(chosenWoj, empSup);
	}
	function checkForFullness(chosenWoj, empSup){
		$.each(empSup.find('> li'), function(index, element){
			var curWojName = $(element.firstChild).text();
			var curWojID;
			
			for(var ID in chosenWoj){
				if(chosenWoj[ID].name == curWojName){
					curWojID = ID.split('.')[2];
					break;
				}
			}
			
			//console.log($(element).find('ul li').length + ' -> ' + numOfRegionsInWoj[curWojID]);
			if($(element).find('ul li').length == numOfRegionsInWoj[curWojID]){
				$(element).find('ul li:first').siblings().remove();
				$(element).find('ul li').first().text("Całe województwo");
			}
		});
	}
	
	/* ------ Mapa ------ */
	function hideAllRegions(){
		markedRegions.length = 0;
		for(subregionID in regions){
			if(subregionID.split('.')[0] == '4'){
				regions[subregionID].attr(unchosen).hide();
			}
			else if(subregionID.split('.')[0] == '2'){
				regions[subregionID].show();
				regions[subregionID].attr(fillVisible);
			}
		}
		$("#showCities").prop("checked", false);
	}
	function showAllRegionsInWoj(wojID){
		for(subregionID in regions){
			if(subregionID.split('.')[2] == wojID){
				if(subregionID.split('.')[0] == '2'){
					regions[subregionID].attr(fillInvisible);
				}
				else{
					if(markedRegions.includes(subregionID))
						continue;
					regions[subregionID].show();
					regions[subregionID].attr(fillVisible);
				}
			}
		}
	}
	function getRandomColor() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}
	function updateMapInDispMode(employees){
		hideAllRegions();
		markedRegions.length = 0;
		for(employee in employees){
			curColor = getRandomColor();
			for(index in employees[employee]['rID']){
				curRID = employees[employee]['rID'][index];
				if(regions[curRID] == null)
					break;
				if(markedRegions.includes(curRID))
					regions[curRID].attr(multipleEmps);
				else{
					showAllRegionsInWoj(curRID.split('.')[2]);
					markedRegions.push(curRID);
					regions[curRID].node.setAttribute('fill', curColor);
				}
			}
		}
	}
	
	/* ----- Prawa kolumna ----- */
	function showVisibleEmployees(toBeVisible){
		editBoxes = {restoreNameBox:{}, nameBox:{}, restoreDepBox:{}, depBox:{}};
		allEmpBox = $('#chooseEmployees');
		allEmpBox.empty();
		for(employee in toBeVisible){(function(employee){
			allEmpBox.append('<div class="employee" id="' + toBeVisible[employee]['eID'] + '" />');
			empBox = $('#'+toBeVisible[employee]['eID']);
			empBox.append('<input type="checkbox" />');
			empBox.append('<p class="empName">'+ toBeVisible[employee]['name'] + ' ' + toBeVisible[employee]['surname'] +'</p>');
			empBox.append('<p class="empDep">' + toBeVisible[employee]['department'] +'</p>');
			empBox.find('input').change(function(){
				
				/* --- ZAZNACZANIE --- */
				if($(this).is(":checked")){
					if(!displayMode){
						$('#chooseEmployees input:checkbox').attr('disabled', 'true');
						$(this).removeAttr('disabled');
						makeBoxesEditable(toBeVisible[employee]['eID']);
					}
					for(check in chosenEmployees){
						if(chosenEmployees[check]['eID'] == toBeVisible[employee]['eID']){
							alert("Zduplikowanie wybranych użytkowników");
							break;
						}
					}
					chosenEmployees.push(toBeVisible[employee]);
				}
				else if(displayMode || confirm("To usunie wszelkie wprowadzone zmiany.\nJesteś pewien?")){
					if(!displayMode){
						$('#chooseEmployees input:checkbox').removeAttr('disabled');
						restoreDefaultBoxes(toBeVisible[employee]['eID']);
					}
					for(check in chosenEmployees){
						if(chosenEmployees[check]['eID'] == toBeVisible[employee]['eID']){
							chosenEmployees[check]['tmp_rID'] = chosenEmployees[check]['rID'].slice();
							chosenEmployees.splice(check, 1);
							break;
						}
					}
				}
				else {
					$(this).prop("checked", true)
				}
				
				updateLeftColumn(chosenEmployees);
				updateMapInDispMode(chosenEmployees);
			});
		})(employee)}
	}
	function fillDepSelect(){
		var select = $("#chooseDep select");
		select.append('<option>Wszystkie</option>');
		for(dep in allDepartments){
			select.append('<option>'+ allDepartments[dep] +'</option>');
		}
		$("option:contains('Wszystkie')").prop('selected', 'selected');
	}
	function restoreDefaultBoxes(eID = 0){
		if(eID == 0){
			for(eID in editBoxes.restoreNameBox){
				editBoxes.nameBox[eID].replaceWith(editBoxes.restoreNameBox[eID]);
				delete editBoxes.restoreNameBox[eID];
				delete editBoxes.nameBox[eID];
			}
			for(eID in editBoxes.restoreDepBox){
				editBoxes.depBox[eID].replaceWith(editBoxes.restoreDepBox[eID]);
				delete editBoxes.restoreDepBox[eID];
				delete editBoxes.depBox[eID];
			}
		}
		else{
			editBoxes.nameBox[eID].replaceWith(editBoxes.restoreNameBox[eID]);
			delete editBoxes.restoreNameBox[eID];
			delete editBoxes.nameBox[eID];
			
			editBoxes.depBox[eID].replaceWith(editBoxes.restoreDepBox[eID]);
			delete editBoxes.restoreDepBox[eID];
			delete editBoxes.depBox[eID];
		}
			
	}	
	function makeBoxesEditable(eID){
		editBoxes.nameBox[eID] = $("#"+eID).find(".empName");
		editBoxes.restoreNameBox[eID] = editBoxes.nameBox[eID].clone();
		editBoxes.nameBox[eID].replaceWith('<input type="text" value="' + editBoxes.restoreNameBox[eID].text() + '"/>');
		editBoxes.nameBox[eID] = $("#"+ eID + " input:text");
		
		editBoxes.depBox[eID] = $("#"+eID).find(".empDep");
		editBoxes.restoreDepBox[eID] = editBoxes.depBox[eID].clone();
		editBoxes.depBox[eID].replaceWith('<select></select>');
		editBoxes.depBox[eID] = $(".employee select");
		for(dep in allDepartments){
			editBoxes.depBox[eID].append('<option>'+ allDepartments[dep] +'</option>');
		}
		$("#"+ eID + " option:contains('"+editBoxes.restoreDepBox[eID].text()+"')").prop('selected', 'selected');
	}	
	
	/* ------ Inne ------ */
	window.onresize = function(){layoutFix();}
	function layoutFix(){
		var headerPos = $('header').position();
		
		$('#center').css('marginLeft', $('#left').outerWidth());
		$('#center').css('marginRight', $('#right').outerWidth());
		if(headerPos.left + $('header').outerWidth(true) <= $(window).width()){
			headerPos = $('header').position();
			$('#right').css('left', headerPos.left + $('header').outerWidth(true) - $('#right').width());
		}
		else{
			headerPos = $('header').position();
			$('#right').css('left', $(window).width() - $('#right').width() - headerPos.left);
		}
		$('#left').css('left', headerPos.left);
		$('#left').css('top', headerPos.top + $('header').outerHeight(true));
		$('#right').css('top', headerPos.top + $('header').outerHeight(true));
		
		$("#showCities").prop("checked", false);
	}
	function getRandomID(){
		return Math.floor((Math.random() * 1000) + 1);
	}
	
	/* --- Guziki mapy i eksportu--- */
	$(".mapButton").click(function(){
		var whichEvent = $(this).attr('id');
		var factor = 1.2;
		if(whichEvent == "zoom-in")
			canvasScale *= factor;
		else if(whichEvent == "zoom-out")
			canvasScale /= factor;
		var viewBoxSize = $("svg").attr('viewBox').split(' '); 
		var newWidth = viewBoxSize[2]*canvasScale;
		var newHeight = viewBoxSize[3]*canvasScale;
		$("svg").attr('width', newWidth).attr('height', newHeight);
		layoutFix();
	});
	$("#showCities").change(function(){
		if(this.checked){
			$.each(cities, function(regID, element){
				element.attr(city).show().toFront();
			});
		}
		else{
			$.each(cities, function(regID, element){
				element.attr(pow).toBack();
				if(markedRegions.includes(regID))
					element.attr(chosen);
				else
					element.attr(unchosen);
			});
		}
	});
	$('#postal-codes').click(function(){
		$.getJSON("regions/kody.json", function(json){
			var allPostalCodes = json;
			var chosenPostalCodes = '';
			for(employee in chosenEmployees){
				chosenPostalCodes += chosenEmployees[employee]['name'] + ' ' + chosenEmployees[employee]['surname'] + ';';
				for(region in chosenEmployees[employee]['tmp_rID']){
					var curName = regionsIdentifiers[chosenEmployees[employee]['tmp_rID'][region]]['name'];
					if(curName in allPostalCodes)
						chosenPostalCodes += allPostalCodes[curName].join(';');
					else
						alert("Nie udało pobrać się danego elementu: " + curName);
				}
				chosenPostalCodes += '\r\n';
			}
			
			var element = document.createElement('a');
			element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(chosenPostalCodes));
			element.setAttribute('download', 'kody-pocztowe.csv');

			element.style.display = 'none';
			document.body.appendChild(element);

			element.click();

			document.body.removeChild(element);
		});
	});
	
	/* --- Guziki podglądu --- */
	$('#displayButton').click(function(){
		displayMode = true;
		chosenEmployees.length = 0;
		hideAllRegions();
		$('#displayWholeToolBox').css('display', 'block');
		$('#editionWholeToolBox').css('display', 'none');
		
		visibleEmployees = allEmployees.slice();
		showVisibleEmployees(visibleEmployees);
		updateLeftColumn(chosenEmployees);
		updateMapInDispMode(chosenEmployees);
	});
	$('#selectAll').click(function(){
		chosenEmployees = visibleEmployees.slice();
		$("input:checkbox").prop('checked', true);
		
		updateLeftColumn(chosenEmployees);
		updateMapInDispMode(chosenEmployees);
	});	
	$('#deselectAll').click(function(){
		chosenEmployees.length = 0;
		$("input:checkbox").prop('checked', false);
		
		updateLeftColumn(chosenEmployees);
		hideAllRegions();
	});
	
	/* --- Guziki edycji --- */
	$('#editionButton').click(function(){
		displayMode = false;
		chosenEmployees.length = 0;
		$('#displayWholeToolBox').css('display', 'none');
		$('#editionWholeToolBox').css('display', 'block');
		
		visibleEmployees = allEmployees.slice();
		showVisibleEmployees(visibleEmployees);
		updateLeftColumn(chosenEmployees);
		updateMapInDispMode(chosenEmployees);
	});
	$('#addUser').click(function(){
		var newID;
		do{
			newID = getRandomID();
			if(allEmployees.length == 0)
				break;
		}while(allEmployees.findIndex(employee => employee['eID'] == newID) == -1);
		
		var newUser = {
			'name': 'imie',
			'surname': 'nazwisko',
			'department': 'dzial',
			'rID': [],
			'tmp_rID': [],
			'eID': getRandomID()
		};
		allEmployees.push(newUser);
		$('#editionButton').trigger('click');
	});
	$('#delUser').click(function(){
		var onlyKey = Object.keys(editBoxes.nameBox)[0];
		indexInAllEmployees = allEmployees.findIndex(employee => employee['eID'] == onlyKey);
		if(indexInAllEmployees == -1)
			alert("Proszę wybrać użytkownika do usunięcia");
		else if(confirm("Czy na pewno usunąć użytkownika \"" + allEmployees[indexInAllEmployees]['name'] + ' ' + allEmployees[indexInAllEmployees]['surname'] + "\"?")) {
			sendEmployeeToDatabase(allEmployees[indexInAllEmployees], true);
			allEmployees.splice(indexInAllEmployees, 1);
			$('#editionButton').trigger('click');
		}
	});
	$('#saveChanges').click(function(evt){
		evt.stopImmediatePropagation();

		if(chosenEmployees.length == 0)
			return;
			
		if(confirm("Czy chcesz zapisać zmiany dotyczące wybranego użytkownika?")){
			var onlyKey = Object.keys(editBoxes.nameBox)[0];
			editBoxes.newName = editBoxes.nameBox[onlyKey].val();
			editBoxes.newDep = editBoxes.depBox[onlyKey].find(":selected").text();
			
			for(var employee in allEmployees){
				if(allEmployees[employee]['eID'] == editBoxes.nameBox[onlyKey].parent().attr('id')){
					allEmployees[employee]['name'] = editBoxes.newName.split(' ')[0];
					allEmployees[employee]['surname'] =  editBoxes.newName.split(' ')[1];
					allEmployees[employee]['department'] = editBoxes.newDep;
					
					allEmployees[employee]['rID'] = allEmployees[employee]['tmp_rID'].slice();
					
					restoreDefaultBoxes(allEmployees[employee]['eID']);
					$('#'+allEmployees[employee]['eID']).find('.empName').text(allEmployees[employee]['name'] + ' ' + allEmployees[employee]['surname']);							
					$('#'+allEmployees[employee]['eID']).find('.empDep').text(allEmployees[employee]['department']);
				
					sendEmployeeToDatabase(allEmployees[employee]);
					
					break;
				}
			}
			$('input:checkbox').removeAttr('disabled');
			$('#deselectAll').trigger($.Event("click"));
		}	
	});
		
	/* --- Narzędzia wyszukiwania --- */
	$('#chooseDep select').change(function(){
		peopleFromChosenDep.length = 0;
		allEmployees.forEach(function(element, index, array){
			element['tmp_rID'] = element['rID'].slice();
		});
		chosenEmployees.length = 0;
		var selectedDep = $(this).find(":selected").val();
		if(selectedDep == $(this).children(":first").val())
			peopleFromChosenDep = allEmployees.slice();
		else{
			for(employee in allEmployees){
				if(allEmployees[employee]['department'] == selectedDep)
					peopleFromChosenDep.push(allEmployees[employee]);
			}
		}
		
		$('#searchBox').trigger('input');
	});
	$('#searchBox').on('input', function(e){
		visibleEmployees.length = 0;
		$.each(peopleFromChosenDep, function(index, element){
			if(e.target.value == undefined || element['name'].toLowerCase().indexOf(e.target.value.toLowerCase() ) != -1 || element['surname'].toLowerCase().indexOf(e.target.value.toLowerCase()) != -1)
				visibleEmployees.push(element);
		});
		
		showVisibleEmployees(visibleEmployees);
		updateLeftColumn();
	});
});
