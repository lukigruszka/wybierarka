	var canvasScale = 1; //Do modyfikowania, gdy chcemy zmienić rozmiar płótna
	var mapWidth = 720;
	var mapHeight = 690;
	var mapAcc = 10;
	var mapMod = 11 - 10*mapAcc/100;
	mapMod = 20;//nie ma być na sztywno
	
	var allDepartments = [
		"drob",
		"trzoda",
		"bydlo",
		"dystrybucja"
	];
	
	var style = {
		fill: "#e0ebeb",
		stroke: "#aaa",
		"stroke-linejoin": "round",
		cursor: "pointer"
	};
	
	var fillVisible = {
		"fill-opacity": "1",
		fill: "#e0ebeb"
	};

	var fillInvisible = {
		"fill-opacity": "0",
		fill: "none"
	};
	
	var pow = {
		"stroke-width": 1
	};
	
	var woj = {
		"stroke-width": 2.5
	};
	
	var city = {
		"stroke-width": 2,
		fill: "red",
	};
	
	var hover = {
		fill: "#ffff99"
	};
	
	var unchosen = {
		fill: "#e0ebeb"
	};
	
	var chosen = {
		fill: "#66ff66"
	};
	
	var multipleEmps = {
		fill: "#ffffff"
	};