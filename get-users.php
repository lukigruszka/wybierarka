﻿<?php
require_once('db-connect.php');

$db = DbConnect();

$query = $db->prepare("SELECT * FROM `users` ORDER BY name, surname");
$query->execute();
$data = $query->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($data);

$db = null;
?>